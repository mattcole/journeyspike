﻿using System.Collections.Generic;
using ChatSpike.Definition;

namespace ChatSpike.Builders
{
    public class JourneyDefinitionBuilder
    {
        private List<QuestionDefinition> _questionDefinitions = new List<QuestionDefinition>();

        private JourneyDefinitionBuilder() { }

        public static JourneyDefinitionBuilder New => new JourneyDefinitionBuilder();

        public QuestionBuilder WithQuestion(string questionText, string answerKey)
        {
            return new QuestionBuilder(questionText, answerKey, _AddQuestion);
        }

        public JourneyDefinition Build()
        {
            return new JourneyDefinition(_questionDefinitions);
        }

        private JourneyDefinitionBuilder _AddQuestion(QuestionDefinition questionDefinition)
        {
            _questionDefinitions.Add(questionDefinition);
            return this;
        }
    }
}