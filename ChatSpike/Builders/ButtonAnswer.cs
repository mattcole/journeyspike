using System.Collections.Generic;
using System.Linq;
using ChatSpike.Definition;

namespace ChatSpike.Builders
{
    public class ButtonAnswer : IAnswers
    {
        private readonly List<ButtonDefinition> _buttonDefinitions;

        public ButtonAnswer(List<ButtonDefinition> buttonDefinitions)
        {
            _buttonDefinitions = buttonDefinitions;
        }
        
        public int GetNextQuestionIdForAnswer(IDictionary<string, object> journeyData, object answer)
        {
            var answerButton = _buttonDefinitions.SingleOrDefault(bd => bd.ButtonValue.Equals(answer));
            Guard.Against<ButtonAnswerNotFoundException>(answerButton == null, $"No answer found with value of {answer}");

            return answerButton.NavigationDeterminer.GetIdOfNextQuestion(journeyData);
        }

        public void Render(IAnswerRenderer renderer)
        {
            foreach (var def in _buttonDefinitions)
            {
                renderer.Render($"({def.ButtonValue}): {def.ButtonText}");
            }
        }
    }
}