﻿using ChatSpike.Definition;

namespace ChatSpike.Builders
{
    public class NumericAnswer : DelegatingNavigationAnswer
    {
        public NumericAnswer(INavigationDeterminer navigator) : base(navigator)
        {
        }

        public override void Render(IAnswerRenderer renderer)
        {
            renderer.Render("(Please supply a numeric value):");
        }
    }
}