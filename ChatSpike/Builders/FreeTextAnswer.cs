﻿using ChatSpike.Definition;

namespace ChatSpike.Builders
{
    public class FreeTextAnswer : DelegatingNavigationAnswer
    {
        public FreeTextAnswer(INavigationDeterminer navigator) : base (navigator)
        {
        }

        public override void Render(IAnswerRenderer renderer)
        {
            renderer.Render("(Please supply a value):");
        }
    }
}