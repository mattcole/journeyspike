﻿using System;
using System.Collections.Generic;
using ChatSpike.Definition;
using ChatSpike.Definition.Navigation;

namespace ChatSpike.Builders
{
    public class QuestionBuilder
    {
        private readonly string _questionText;
        private readonly string _answerKey;
        private readonly Func<QuestionDefinition, JourneyDefinitionBuilder> _addQuestionToParent;
        private int _order;
        private bool _goesToNextQuestion;
        private AnswerType _answerType = AnswerType.None;

        private List<ButtonDefinition> _buttonDefinitions;
        private INavigationDeterminer _navigationDeterminer;
        private List<ICustomDataProcessor> _customDataProcessors = new List<ICustomDataProcessor>();
        private bool _isFinalQuestionInJourney;

        public QuestionBuilder(string questionText, string answerKey, Func<QuestionDefinition, JourneyDefinitionBuilder> addQuestionToParent)
        {
            _questionText = questionText;
            _answerKey = answerKey;
            _addQuestionToParent = addQuestionToParent;
        }

        public QuestionBuilder WithOrder(int order)
        {
            _order = order;
            return this;
        }

        public QuestionBuilder WithFreeTextAnswer()
        {
            Guard.Against(_answerType != AnswerType.None, "You've already specified an answer type");
            _answerType = AnswerType.FreeText;
            return this;
        }

        public QuestionBuilder WithNumericAnswer()
        {
            Guard.Against(_answerType != AnswerType.None, "You've already specified an answer type");
            _answerType = AnswerType.Numeric;
            return this;
        }

        public QuestionBuilder ThatGoesToNextQuestion()
        {
            _goesToNextQuestion = true;
            return this;
        }

        public ButtonAnswersBuilder WithButtonAnswers()
        {
            Guard.Against(_answerType != AnswerType.None, "You've already specified an answer type");
            _answerType = AnswerType.Buttons;
            return new ButtonAnswersBuilder(_AddButtons);
        }

        public QuestionBuilder WithCustomNavigation(INavigationDeterminer navigationDeterminer)
        {
            _navigationDeterminer = navigationDeterminer;
            return this;
        }

        public QuestionBuilder WithCustomDataProcessor(ICustomDataProcessor processor)
        {
            _customDataProcessors.Add(processor);
            return this;
        }

        public QuestionBuilder ThatIsTheFinalQuestion()
        {
            _isFinalQuestionInJourney = true;
            return this;
        }

        public JourneyDefinitionBuilder Add()
        {
            _EnsureNavigationIsSet();
            var answers = _BuildAnswers();

            var questionDefinition = new QuestionDefinition(_order, _questionText, answers, _answerKey,
                _isFinalQuestionInJourney, _customDataProcessors);

            return _addQuestionToParent(questionDefinition);
        }

        private void _EnsureNavigationIsSet()
        {
            Guard.Against(_answerType != AnswerType.Buttons 
                && !_isFinalQuestionInJourney
                && !_goesToNextQuestion 
                && _navigationDeterminer == null, $"Must specify a navigation type for {_questionText}");
        }

        private IAnswers _BuildAnswers()
        {
            var navigator = _goesToNextQuestion
                ? new GoToQuestionNavigator(_order + 1)
                : _navigationDeterminer;

            switch (_answerType)
            {
                case AnswerType.FreeText: return new FreeTextAnswer(navigator);
                case AnswerType.Numeric: return new NumericAnswer(navigator);
                case AnswerType.Buttons: return new ButtonAnswer(_buttonDefinitions);
                default: throw new ArgumentException("Must specify an answer type when creating a question");
            }
        }

        private QuestionBuilder _AddButtons(List<ButtonDefinition> buttonDefinitions)
        {
            _buttonDefinitions = buttonDefinitions;
            return this;
        }
    }
}