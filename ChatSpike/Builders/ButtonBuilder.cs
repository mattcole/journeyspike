﻿using System;
using ChatSpike.Definition;

namespace ChatSpike.Builders
{
    public class ButtonBuilder
    {
        private readonly object _buttonValue;
        private readonly string _buttonText;
        private readonly Func<ButtonDefinition, ButtonAnswersBuilder> _addAndReturnParent;
        private readonly ButtonAnswersBuilder _parentBuilder;
        private int _questionIdToNavigateTo;
        private INavigationDeterminer _navigationDeterminer;

        public ButtonBuilder(string buttonText, object buttonValue, Func<ButtonDefinition, ButtonAnswersBuilder> addAndReturnParent)
        {
            _buttonValue = buttonValue;
            _buttonText = buttonText;
            _addAndReturnParent = addAndReturnParent;
        }

        public ButtonBuilder ThatGoesToQuestion(int questionId)
        {
            Guard.Against(questionId == default(int), "Must supply a question id to navigate to");
            _questionIdToNavigateTo = questionId;
            return this;
        }

        public ButtonBuilder WithCustomNavigation(INavigationDeterminer navigationDeterminer)
        {
            _navigationDeterminer = navigationDeterminer;
            return this;
        }

        public ButtonAnswersBuilder Add()
        {
            var buttonDefinition = _questionIdToNavigateTo != default(int)
                ? ButtonDefinition.CreateDefinitionWithSimpleNavigation(_buttonText, _buttonValue, _questionIdToNavigateTo)
                : ButtonDefinition.CreateDefinitionWithCustomNavigation(_buttonText, _buttonValue, _navigationDeterminer);

            return _addAndReturnParent(buttonDefinition);
        }
    }
}