﻿using System;
using System.Collections.Generic;
using ChatSpike.Definition;

namespace ChatSpike.Builders
{
    public class ButtonAnswersBuilder
    {
        private readonly Func<List<ButtonDefinition>, QuestionBuilder> _addFunction;
        private List<ButtonDefinition> _buttonDefinitions = new List<ButtonDefinition>();

        public ButtonAnswersBuilder(Func<List<ButtonDefinition>, QuestionBuilder> addFunction)
        {
            _addFunction = addFunction;
        }

        public ButtonBuilder WithButton(string buttonText, object buttonValue)
        {
            return new ButtonBuilder(buttonText, buttonValue, _AddButton);
        }

        private ButtonAnswersBuilder _AddButton(ButtonDefinition buttonDefinition)
        {
            _buttonDefinitions.Add(buttonDefinition);
            return this;
        }

        public QuestionBuilder AddButtons()
        {
            return _addFunction(_buttonDefinitions);
        }
    }
}