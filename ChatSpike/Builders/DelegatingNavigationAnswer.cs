﻿using System.Collections.Generic;
using ChatSpike.Definition;

namespace ChatSpike.Builders
{
    public abstract class DelegatingNavigationAnswer : IAnswers
    {
        private readonly INavigationDeterminer _navigator;
        public abstract void Render(IAnswerRenderer renderer);

        protected DelegatingNavigationAnswer(INavigationDeterminer navigator)
        {
            _navigator = navigator;
        }
        public int GetNextQuestionIdForAnswer(IDictionary<string, object> journeyData, object answer)
        {
            return _navigator.GetIdOfNextQuestion(journeyData);
        }

    }
}