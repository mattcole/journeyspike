using System;

namespace ChatSpike.Builders
{
    public class ButtonAnswerNotFoundException : ArgumentException
    {
        public ButtonAnswerNotFoundException(string message) : base(message)
        {
            
        }
    }
}