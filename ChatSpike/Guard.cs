﻿using System;
using System.Reflection;

namespace ChatSpike
{
    public class Guard
    {
        public static void Against(bool predicate, string message)
        {
            if(predicate) throw new InvalidOperationException(message);
        }

        public static void Against<TException>(bool predicate, string message) where TException : Exception
        {
            var exception = (TException) Activator.CreateInstance(typeof(TException), message);
            if (predicate) throw exception;
        }

    }
}