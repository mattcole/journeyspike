﻿namespace ChatSpike.Journeys.TargetNonTarget
{
    public static class AnswerKeys
    {
        public static string InvestmentName = "investment_name";
        public static string TargetOrNonTarget = "target_non_target";
        public static string TargetAmount = "target_amount";
        public static string DepositOrNot = "deposit_yes_no";
        public static string DepositAmount = "deposit_amount";
        public static string MonthlyContribution = "monthly_contribution";
        public static string InflationIncreaseOrNot = "inflation_increase_yes_no";
        public static string InflationAmount = "inflation_amount";
        public static string IncreaseAmountToAdjustForInflationOrNot = "increase_amount_to_adjust_for_inflation_yes_no";
        public static string TargetIncludingInflation = "target_including_inflation";
    }
}