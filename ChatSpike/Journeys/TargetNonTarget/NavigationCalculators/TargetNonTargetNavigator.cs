﻿using System.Collections.Generic;
using ChatSpike.Definition;

namespace ChatSpike.Journeys.TargetNonTarget.NavigationCalculators
{
    public class TargetNonTargetNavigator : INavigationDeterminer
    {
        public int GetIdOfNextQuestion(IDictionary<string, object> journeyData)
        {
            Guard.Against(!journeyData.ContainsKey(AnswerKeys.TargetOrNonTarget), $"No response found for {AnswerKeys.TargetOrNonTarget}");

            return journeyData[AnswerKeys.TargetOrNonTarget].Equals("yes") ? 6 : 10;
        }
    }
}