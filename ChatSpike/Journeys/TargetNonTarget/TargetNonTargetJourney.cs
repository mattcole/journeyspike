﻿using ChatSpike.Builders;
using ChatSpike.Definition;
using ChatSpike.Journeys.TargetNonTarget.CustomDataProcessors;
using ChatSpike.Journeys.TargetNonTarget.NavigationCalculators;

namespace ChatSpike.Journeys.TargetNonTarget
{
    public static class TargetNonTargetJourney
    {
        public static JourneyDefinition JourneyDefinition
        {
            get
            {
                return JourneyDefinitionBuilder.New
                .WithQuestion("Let's start by giving your investment a name", AnswerKeys.InvestmentName)
                    .WithOrder(1)
                    .WithFreeTextAnswer()
                    .ThatGoesToNextQuestion()
                    .Add()
                .WithQuestion("Do you have a target in mind", AnswerKeys.TargetOrNonTarget)
                    .WithOrder(2)
                    .WithButtonAnswers()
                        .WithButton("Yes I do", "yes").ThatGoesToQuestion(3).Add()
                        .WithButton("No I don't", "no").ThatGoesToQuestion(4).Add()
                        .AddButtons()
                    .Add()
                .WithQuestion("How much target?", AnswerKeys.TargetAmount)
                    .WithOrder(3)
                    .WithNumericAnswer()
                    .ThatGoesToNextQuestion()
                    .Add()
                .WithQuestion("Do you have a deposit to start with?", AnswerKeys.DepositOrNot)
                    .WithOrder(4)
                    .WithButtonAnswers()
                        .WithButton("Yes I do", "yes").ThatGoesToQuestion(5).Add()
                        .WithButton("No I don't", "no").WithCustomNavigation(new TargetNonTargetNavigator()).Add()
                        .AddButtons()
                    .Add()
                .WithQuestion("How much deposit?", AnswerKeys.DepositAmount)
                    .WithOrder(5)
                    .WithNumericAnswer()
                    .WithCustomNavigation(new TargetNonTargetNavigator())
                    .Add()
                .WithQuestion("How much can you contribute monthly?", AnswerKeys.MonthlyContribution)
                    .WithOrder(6)
                    .WithNumericAnswer()
                    .ThatGoesToNextQuestion()
                    .Add()
                .WithQuestion("Would you like to increase by inflation %?", AnswerKeys.InflationIncreaseOrNot)
                    .WithOrder(7)
                    .WithButtonAnswers()
                        .WithButton("Yes I do", "yes").ThatGoesToQuestion(8).Add()
                        .WithButton("No I don't", "no").ThatGoesToQuestion(10).Add()
                        .AddButtons()
                    .Add()
                .WithQuestion("How much inflation?", AnswerKeys.InflationAmount)
                    .WithOrder(8)
                    .WithNumericAnswer()
                    .ThatGoesToNextQuestion()
                    .WithCustomDataProcessor(new CalculateInflationAmountProcessor())
                    .Add()
                .WithQuestion("Would you like to increase your target to {{" + AnswerKeys.TargetIncludingInflation + "}} to deal with inflation?", AnswerKeys.IncreaseAmountToAdjustForInflationOrNot)
                    .WithOrder(9)
                    .WithButtonAnswers()
                        .WithButton("Yes I do", "yes").ThatGoesToQuestion(10).Add()
                        .WithButton("No I don't", "no").ThatGoesToQuestion(10).Add()
                        .AddButtons()
                    .ThatGoesToNextQuestion()
                    .Add()
                .WithQuestion("This is the final question just to show divergent paths", "not_a_real_key")
                    .WithOrder(10)
                    .WithFreeTextAnswer()
                    .ThatIsTheFinalQuestion()
                    .Add()
                .Build();
            }
        }
    }
}