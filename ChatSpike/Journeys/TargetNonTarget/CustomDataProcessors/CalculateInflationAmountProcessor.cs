﻿using System;
using System.Collections.Generic;
using ChatSpike.Definition;

namespace ChatSpike.Journeys.TargetNonTarget.CustomDataProcessors
{
    public class CalculateInflationAmountProcessor : ICustomDataProcessor
    {
        public IDictionary<string, object> GenerateCustomDataToAdd(IDictionary<string, object> existingData)
        {
            var preInflationTarget = Convert.ToInt32(existingData[AnswerKeys.TargetAmount]);
            var inflationAdjustmentPercentage = Convert.ToInt32(existingData[AnswerKeys.InflationAmount]);

            var postInflationTarget = preInflationTarget*(1.0 + (inflationAdjustmentPercentage/100.0));

            return new Dictionary<string, object>
            {
                {AnswerKeys.TargetIncludingInflation, postInflationTarget}
            };
        }
    }
}