﻿using System;
using ChatSpike.Builders;
using ChatSpike.Definition;
using ChatSpike.Journeys.TargetNonTarget;
using ChatSpike.Journeys.TargetNonTarget.CustomDataProcessors;
using ChatSpike.Journeys.TargetNonTarget.NavigationCalculators;

namespace ChatSpike
{
    class Program
    {
        private static IAnswerRenderer _answerRenderer = new ConsoleRenderer();
        private static JourneyInstance _journeyInstance;

        static void Main(string[] args)
        {
            _journeyInstance = new JourneyInstance(TargetNonTargetJourney.JourneyDefinition);

            var currentQuestion = _journeyInstance.GetCurrentQuestion();

            while (!currentQuestion.IsFinalQuestionInJourney)
            {
                _DisplayQuestionAndAnswers(currentQuestion);
                var answer = Console.ReadLine();
                try
                {
                    currentQuestion = _journeyInstance.SaveResponseAndGetNextQuestion(answer);
                }
                catch (ButtonAnswerNotFoundException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            Console.WriteLine("Data that was captured:");
            _WriteDictionary(_journeyInstance);
            Console.ReadLine();
        }
        
        private static void _WriteDictionary(JourneyInstance journeyInstance)
        {
            journeyInstance.JourneyData.Keys.Each(key => Console.WriteLine($"{key}: {journeyInstance.JourneyData[key]}"));
        }

        private static void _DisplayQuestionAndAnswers(QuestionDefinition currentQuestion)
        {
            Console.WriteLine(_GetInterpolatedText(currentQuestion.QuestionText));
            currentQuestion.RenderAnswers(_answerRenderer);
        }

        private static string _GetInterpolatedText(string answer)
        {
            var interpolationStartIdx = answer.IndexOf("{{");
            var interpolationEndIdx = answer.IndexOf("}}");

            if (interpolationStartIdx < 0 || interpolationEndIdx < 0) return answer;
            var interpolationKey = answer.Substring(interpolationStartIdx + 2,
                interpolationEndIdx - interpolationStartIdx - 2);

            var valueToInject = _journeyInstance.JourneyData[interpolationKey].ToString();
            return answer.Replace("{{" + interpolationKey + "}}", valueToInject);

        }
    }
}
