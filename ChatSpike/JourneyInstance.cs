using System;
using System.Collections.Generic;
using System.Linq;
using ChatSpike.Definition;

namespace ChatSpike
{
    public class JourneyInstance
    {
        private int _currentQuestionId;
        private JourneyDefinition _journeyDefinition;
        public IReadOnlyDictionary<string, object> JourneyData => _journeyData as IReadOnlyDictionary<string, object>;
        public IDictionary<string, object> _journeyData = new Dictionary<string, object>();

        public JourneyInstance(JourneyDefinition journeyDefinition)
        {
            Guard.Against(journeyDefinition.QuestionDefinitions.All(qd => qd.QuestionId != 1), "Must have question with id of 1 to start journey");
            _journeyDefinition = journeyDefinition;
            _currentQuestionId = 1;
        }

        public QuestionDefinition SaveResponseAndGetNextQuestion<TResponseType>(TResponseType response)
        {
            var question = GetCurrentQuestion();

            _journeyData[question.AnswerKey] = response;
            question.CustomDataProcessors.Each(p =>
            {
                var newData = p.GenerateCustomDataToAdd(_journeyData);
                newData.Keys.Each(key => _journeyData[key] = newData[key]);
            });

            _currentQuestionId = question.GetNextQuestionIdForAnswer(_journeyData, response);

            return GetCurrentQuestion();
        }

        public QuestionDefinition GetCurrentQuestion()
        {
            var question = _journeyDefinition.QuestionDefinitions.SingleOrDefault(qd => qd.QuestionId == _currentQuestionId);
            Guard.Against(question == null, $"Could not find question with id: {_currentQuestionId}");
            return question;
        }
    }
}