﻿using ChatSpike.Definition.Navigation;

namespace ChatSpike.Definition
{
    public class ButtonDefinition
    {
        public string ButtonText { get; private set; }
        public object ButtonValue { get; private set; }
        public INavigationDeterminer NavigationDeterminer { get; private set; }

        private ButtonDefinition() { }

        public static ButtonDefinition CreateDefinitionWithSimpleNavigation(string buttonText, object buttonValue, int nextQuestionId)
        {
            return CreateDefinitionWithCustomNavigation(buttonText, buttonValue, new GoToQuestionNavigator(nextQuestionId));
        }

        public static ButtonDefinition CreateDefinitionWithCustomNavigation(string buttonText, object buttonValue, INavigationDeterminer navigationDeterminer)
        {
            return new ButtonDefinition
            {
                ButtonText = buttonText,
                ButtonValue = buttonValue,
                NavigationDeterminer = navigationDeterminer
            };
        }
    }
}