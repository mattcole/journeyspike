﻿using System;
using System.Collections.Generic;

namespace ChatSpike.Definition
{
    public class ConsoleRenderer : IAnswerRenderer
    {
        public void Render(string answer)
        {
            Console.WriteLine(answer);
        }
    }
}