﻿using System.Collections.Generic;

namespace ChatSpike.Definition
{
    public class QuestionDefinition
    {
        public IAnswers Answers { get; private set; }

        public string QuestionText { get; private set; }

        public int QuestionId { get; private set; }
        public string AnswerKey { get; private set; }
        public bool IsFinalQuestionInJourney { get; private set; }
        public IEnumerable<ICustomDataProcessor> CustomDataProcessors { get; private set; }


        public QuestionDefinition (int questionId, string questionText, IAnswers answers, string answerKey, 
            bool isFinalQuestionInJourney, IEnumerable<ICustomDataProcessor> customDataProcessors)
        {
            Guard.Against(questionId == default(int), "Must supply an id to a question");

            QuestionId = questionId;
            QuestionText = questionText;
            Answers = answers;
            AnswerKey = answerKey;
            IsFinalQuestionInJourney = isFinalQuestionInJourney;
            CustomDataProcessors = customDataProcessors;
        }

        public int GetNextQuestionIdForAnswer(IDictionary<string, object> journeyData, object response)
        {
            return Answers.GetNextQuestionIdForAnswer(journeyData, response);
        }

        public void RenderAnswers(IAnswerRenderer answerRenderer)
        {
            Answers.Render(answerRenderer);
        }
    }
}