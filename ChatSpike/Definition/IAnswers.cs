﻿using System.Collections;
using System.Collections.Generic;

namespace ChatSpike.Definition
{
    public interface IAnswers
    {
        int GetNextQuestionIdForAnswer(IDictionary<string, object> journeyData, object answer);

        void Render(IAnswerRenderer renderer);
    }
}