﻿using System.Collections.Generic;

namespace ChatSpike.Definition.Navigation
{
    public class GoToQuestionNavigator : INavigationDeterminer
    {
        private readonly int _questionToGoTo;

        public GoToQuestionNavigator(int questionToGoTo)
        {
            _questionToGoTo = questionToGoTo;
        }

        public int GetIdOfNextQuestion(IDictionary<string, object> journeyData)
        {
            return _questionToGoTo;
        }
    }
}