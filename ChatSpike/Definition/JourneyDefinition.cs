using System.Collections.Generic;

namespace ChatSpike.Definition
{
    public class JourneyDefinition
    {
        public IEnumerable<QuestionDefinition> QuestionDefinitions { get; private set; }

        public JourneyDefinition(IEnumerable<QuestionDefinition> questionDefinitions)
        {
            QuestionDefinitions = questionDefinitions;
        }
    }
}