namespace ChatSpike.Definition
{
    public enum AnswerType
    {
        None = 0,
        Numeric,
        FreeText,
        Buttons
    }
}