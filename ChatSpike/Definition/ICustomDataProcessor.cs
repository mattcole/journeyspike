using System.Collections.Generic;

namespace ChatSpike.Definition
{
    public interface ICustomDataProcessor
    {
        IDictionary<string, object> GenerateCustomDataToAdd(IDictionary<string, object> existingData);
    }
}