﻿using System.Collections.Generic;

namespace ChatSpike.Definition
{
    public interface IAnswerRenderer
    {
        void Render(string answer);
    }
}