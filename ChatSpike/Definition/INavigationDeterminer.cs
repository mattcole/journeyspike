using System.Collections.Generic;

namespace ChatSpike.Definition
{
    public interface INavigationDeterminer
    {
        int GetIdOfNextQuestion(IDictionary<string, object> journeyData);
    }
}