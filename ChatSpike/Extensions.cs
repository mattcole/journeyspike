﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChatSpike
{
    public static class Extensions
    {
        public static bool MoreThanOneIsTrue(IEnumerable<bool> values)
        {
            return values.Count(b => b) > 1;
        }

        public static void Each<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (var item in items)
            {
                action(item);
            }
        }
    }
}